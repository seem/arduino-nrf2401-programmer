##########################
CROSSTOOL  = avr-
#list all source files [*.c]
SRCS       = main.c
SRCS      += printf_port.c 
SRCS      += nrf24_port.c nrf24_device.c
#the last binary file
BIN        = m328p_nrf_programmer
#MCU name
MCU        = atmega328p
# Processor frequency.
#     This will define a symbol, F_CPU, in all source code files equal to the 
#     processor frequency. You can then use this symbol in your source code to 
#     calculate timings. Do NOT tack on a 'UL' at the end, this will be done
#     automatically to create a 32-bit value in your source code.
F_CPU      = 16000000
#
VPATH      = log wireless stk500v2
#
CC         = $(CROSSTOOL)gcc
OBJDUMP    = $(CROSSTOOL)objdump
OBJCOPY    = $(CROSSTOOL)objcopy
SIZE       = $(CROSSTOOL)size
#
ifeq ($(shell uname -s), Linux)
	DEVICE_PORT = /dev/ttyUSB1#ACM0  #linux
else
	DEVICE_PORT = COM9          #windows 
endif
##########################
#saving  path for *.o *.d
TMPPATH    =  tmp
#saving  path for binary file
BINPATH    =  bin
OBJS       = $(SRCS:%.c=$(TMPPATH)/%.o)
TARGET     = $(BINPATH)/$(BIN)
##########################
MF_ABS_PATH = $(shell pwd)
#
CFLAG_INC   = $(patsubst %,-I%,$(VPATH))
# Place -D or -U options here for C sources
CDEFS = -DF_CPU=$(F_CPU)UL -D__BUILD_TIME__=\"$(shell date "+%Y-%m-%d\ %H:%M:%S")\"
#编译配置项
CFLAGS     = -Wall -Os -std=gnu99 -fshort-enums -g
ifdef MCU
	CFLAGS += -mmcu=$(MCU)
	CFLAGS += $(CDEFS)
endif
CFLAGS    += $(CFLAG_INC)
##########################
#链接配置项
LFLAGS     = -Wl,-Map=$(TARGET).map,--cref
# printf version : 最简化模式，不能识别浮点输出
#LFLAGS    += -Wl,-u,vfprintf -lprintf_min
# Minimalistic scanf version
#LFLAGS    += -Wl,-u,vfscanf -lscanf_min


all: dirs info $(TARGET)
#
dirs:
	@mkdir -p $(BINPATH) $(TMPPATH)
#
info:
	@echo CFLAGS = $(CFLAGS)
	@echo LFLAGS = $(LFLAGS)
	@echo ================================================
#
$(TARGET): % :%.elf
	@echo ================================================
	@#ELF info
	$(OBJDUMP) -x -d $< > $@.dump
	@#
	$(OBJDUMP) -S -I -z $< > $@.s
	@#output binary image
	$(OBJCOPY) -j .text -j .data -O binary $< $@
	$(OBJCOPY) -j .text -j .data -O ihex $< $@.hex
	@#show memory usage
	@echo ================================================
	@$(SIZE) $<
	@echo ================================================
#link
%.elf: $(OBJS)
	@echo Linking [$@] $^
	@$(CC) $(CFLAGS) $(LFLAGS) -o $@ $^
#compile
$(TMPPATH)/%.o: %.c
	@echo Compiling [$<]
	@$(CC) -c $(CFLAGS) -MMD -MP -MF $(@:.o=.d) -o $@ $<
	@$(OBJDUMP) -S $@ > $(@:.o=.s)
upload:
	@#avrdude -c arduino -p $(MCU) -P $(DEVICE_PORT) -U flash:r:$(BINPATH)/arduino.hex:i
	avrdude -c arduino -p $(MCU) -P $(DEVICE_PORT)  -e -U flash:w:$(TARGET).hex:i
clean:
	-rm -f -R $(BINPATH)
	-rm -f -R $(TMPPATH)

#include dependency files
-include $(SRCS:%.c=$(TMPPATH)/%.d)
.PHONY: all clean upload dirs