# arduino-nrf2401-programmer

#### 介绍
这是arduino-nrf2401-bootloader(项目链接: https://gitee.com/alicedodo/arduino-nrf2401-bootloader     )的配套项目.      
在原项目中虽然提供了编程器端的实现代码,但那只是基于本项目拼凑出来的可在arduino IDE中使用的简化版,这里才是真身.    
programmer后续的更新都只在这个项目中实现,对于那个同名的ino文件,除非有BUG非改不可,基本不会去更新了.     
      
本项目是一个常规的纯C语言的avr-gcc+Makefile的单片机项目,未使用任何arduino库,所以不加修改的话,无法在arduino IDE中编译.

