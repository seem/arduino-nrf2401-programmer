#ifndef _PRINTF_PORT_H_
#define _PRINTF_PORT_H_
#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
//
#define LOG_LEVEL_NONE   0
#define LOG_LEVEL_NORMAL 1
#define LOG_LEVEL_DEBUG  2
//	
#define LOG_LEVEL_SET 0
//
#define LOG(level, fmt_str, ...) 	do                                           \
									{                                            \
										if(LOG_LEVEL_SET>=level)                 \
	                             		{printf_P(PSTR(fmt_str), ##__VA_ARGS__);}\
	                             	}while(0)

//
#define PRINT(fmt_str, ...)         do                                           \
									{                                            \
										if(LOG_LEVEL_SET>=LOG_LEVEL_NORMAL)      \
	                             		{printf_P(PSTR(fmt_str), ##__VA_ARGS__);}\
	                             	}while(0)

#if LOG_LEVEL_SET>=LOG_LEVEL_DEBUG
extern void log_hex_dump(char*, int);
#define DUMP(src, sz) log_hex_dump(src, sz)
#else
#define DUMP(src, sz) do{/*do nothing*/}while(0)
#endif

extern char log_uart_getchar(void);
extern void log_init(void);

#ifdef __cplusplus
}
#endif
#endif
