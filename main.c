#include <inttypes.h>
#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include "stk500v2_cmd.h"
#include "nrf24_device.h"
#include "printf_port.h"

//enable/disable nrf24l01p-freq-hopping
#define NRF_ENABLE_FREQ_HOPPING    1     // 0 - disable / >0 - enable
#if NRF_ENABLE_FREQ_HOPPING
	//freq-hopping need some random value,so we get random seed by 
	//reading noise signal on a ADC pin, NRF_RANDOM_SEED_ADC_PIN defines
	//the target pin number (range: PIN0~PIN5)
	//Note: the target ADC pin MUST connect nothing!
	#define NRF_RANDOM_SEED_ADC_PIN    PIN0
	//air-data-rate after freq-hopping
	#define NRF_FREQ_HOPPING_AIR_DATA_RATE_SET  0     // 0-2Mbps / 1-1Mbps / 2-250Kbps
	//lowest freq (or left freq boundary) when generating RF channel
	//we will get a random RF channel between NRF_FREQ_HOPPING_LOWEST_CHANNEL and 2524 MHz
    #define NRF_FREQ_HOPPING_LOWEST_CHANNEL    2490  //MHz
#endif
//Maximum number of TX retransmits, range: 0~15
#define NRF_RETRANSMIT_MAX        5
//enable/disable nrf24l01p packet-loss statistics
#define NRF_ENABLE_PACKLOSS_CNT   1    // 0 - disable / >0 - enable

#define NRF_DEF_ADR_BYTE           0xA5   
#define NRF_DEF_AIR_DATA_RATE_SET  2     // 0-2Mbps / 1-1Mbps / 2-250Kbps
#define NRF_DEF_RF_CHANNEL         2522  // 2400~2525 MHz

void flash_copy(uint16_t flsh_adr, uint8_t* ram_adr, uint16_t sz)
{
	while(sz)
	{
		*ram_adr = pgm_read_byte(flsh_adr);
		flsh_adr++;
		ram_adr++;
		sz--;
	}
}

void ram_copy(uint8_t* dest, uint8_t* src, uint8_t sz)
{
	while(sz)
	{
		*dest = *src;
		dest++;
		src++;
		sz--;
	}
}

void uart_init(void)
{
	#define BAUD 115200
	#define BAUD_TOL 3
   	#include <util/setbaud.h>
	//初始化。工作模式，帧结构等（UCSRnC）
	UCSR0C = (0<<UMSEL01)|(0<<UMSEL00)|0x06; //异步，8位数据，无奇偶校验，一个停止位 10000110
	//波特率的设置。（UBRRnL ,UBRRnH）
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE; 
	#if USE_2X
		UCSR0A |= (1<<U2X0);
	#else
		UCSR0A &= ~(1<<U2X0);
	#endif
	UCSR0B = (uint8_t)(1<<TXEN0)|(1<<RXEN0);//仅使能发送和接收
}

void uart_putbyte(uint8_t c)
{
	while(!(UCSR0A&(1<<UDRE0)));
	UDR0 = c;
}

uint8_t uart_getbyte(void)
{
	while(!(UCSR0A&(1<<RXC0))); //等待接收到新数据
	return UDR0;
}

#if NRF_ENABLE_PACKLOSS_CNT
unsigned int pack_sum      = 0;
unsigned int pack_loss_cnt = 0;
#define packloss_get()    (pack_loss_cnt)
#define packloss_set(cnt) do{ pack_loss_cnt=(cnt); }while(0)
#define packloss_add(cnt) do{ pack_loss_cnt+=(cnt); }while(0)
#define packsum_get()     (pack_sum)
#define packsum_set(cnt)  do{ pack_sum=(cnt); }while(0)
#define packsum_add(cnt)  do{ pack_sum+=(cnt); }while(0)
#endif

#if NRF_DEF_AIR_DATA_RATE_SET==0
	#define NRF_AIR_DATA_RATE NRF_VAL_RF_DR_2Mbps
#elif NRF_DEF_AIR_DATA_RATE_SET==1
	#define NRF_AIR_DATA_RATE NRF_VAL_RF_DR_1Mbps
#elif NRF_DEF_AIR_DATA_RATE_SET==2
	#define NRF_AIR_DATA_RATE NRF_VAL_RF_DR_250kbps
#endif
#if NRF_AIR_DATA_RATE==NRF_VAL_RF_DR_2Mbps
	#define NRF_AUTO_RETRANS_DELAY NRF_VAL_ARD_500uS
#elif NRF_AIR_DATA_RATE==NRF_VAL_RF_DR_1Mbps
	#define NRF_AUTO_RETRANS_DELAY NRF_VAL_ARD_750uS
#elif NRF_AIR_DATA_RATE==NRF_VAL_RF_DR_250kbps
	#define NRF_AUTO_RETRANS_DELAY NRF_VAL_ARD_1750uS
#endif
void nrf_set_default_addr(void)
{
	char adr[5];
	static const char adr_rom[5] PROGMEM = 
	{//MSByte first
		NRF_DEF_ADR_BYTE, NRF_DEF_ADR_BYTE, NRF_DEF_ADR_BYTE, NRF_DEF_ADR_BYTE, NRF_DEF_ADR_BYTE
	}; 
	flash_copy((uint16_t)adr_rom, (uint8_t*)adr, 5);
	nrf_set_addr_wid(NRF_VAL_AW_5Bytes);
	nrf_set_pipe_addr(NRF_PIPE0, adr, 5);
	nrf_set_tx_addr(adr, 5);
}

void nrf_init(void)
{
	NRF_SPI_INIT();
	NRF_CE_LOW();
	NRF_CSN_HIGH();
	NRF_DELAY_mS(120); //wait for power on reset
	//config as PTX
	nrf_set_primary_mode(0); 
	nrf_set_rf_channel(NRF_DEF_RF_CHANNEL);
	nrf_set_rf_data_rate(NRF_AIR_DATA_RATE);
	nrf_set_auto_retrans(NRF_AUTO_RETRANS_DELAY, NRF_RETRANSMIT_MAX);
	nrf_set_default_addr();
	nrf_pipe_enable(NRF_PIPE0);
	nrf_tx_flush();
	nrf_rx_flush();
	nrf_clear_irq_src(0x07);
	nrf_power_up();
}
void nrf_clear(void)
{
	nrf_tx_flush();
	nrf_rx_flush();
	nrf_clear_irq_src(0x07);
}
uint8_t nrf_wait_for_irq(void)
{	//wait for IRQ pin active
	while(0!=NRF_IRQ_PIN());
#if NRF_ENABLE_PACKLOSS_CNT
	uint8_t cnt = NRF_OBSERVE_ARC(nrf_reg_read_byte(NRF_REG_OBSERVE_TX));
	packloss_add(cnt);
	if(cnt>0)
		LOG(LOG_LEVEL_NORMAL, "obsrv: %d , %d\r\n", cnt, packloss_get());
	if(cnt<NRF_RETRANSMIT_MAX)
	{
		cnt++; //last re-transmit success
	}
	packsum_add(cnt);
#endif
	return 0;
}
void nrf_sending_start(void)
{
	NRF_CE_HIGH();
	NRF_DELAY_uS(12);
	NRF_CE_LOW();
}

uint8_t stk500v2_cal_checksum(uint8_t* frame_buffer, uint16_t frame_len_no_cksum)
{
	uint8_t cksum = 0x00;

	uint8_t* pbuf = frame_buffer;
	uint16_t sz = frame_len_no_cksum - 5;
	frame_buffer[2] = sz>>8;  //SIZE1
	frame_buffer[3] = sz&0xFF;//SIZE0

	while(frame_len_no_cksum)
	{
		cksum ^= *pbuf;
		pbuf++;
		frame_len_no_cksum--;
	}
	return cksum;
}
//
//frame state machine
#define SM_START     0
#define SM_SEQ       1
#define SM_SIZE1     2
#define SM_SIZE0     3
#define SM_TOKEN     4
#define SM_DATA      5
#define SM_CHECKSUM  6
#define sm_reset()          do{ sm=SM_START; frame_len_no_cksum=0; frame_datacnt=0; frame_ptr=frame_buffer; }while(0)
#define buf_append_byte(ch) do{ *frame_ptr++ = ch; }while(0)
#define buf_get_byte(idx)   (frame_buffer[idx])
uint16_t stk500v2_get_frame(uint8_t* frame_buffer, uint16_t bufsz)
{
	uint8_t  sm;
	uint8_t* frame_ptr;
	uint16_t frame_len_no_cksum;
	uint16_t frame_datacnt;
	sm_reset();
	while(1)
	{
		uint8_t ch = uart_getbyte();
		buf_append_byte(ch);
		switch(sm)
		{
			case SM_START:
				{
					if(MESSAGE_START==ch)
						sm = SM_SEQ;
					else
						sm_reset();
				}
				break;
			case SM_SEQ:
				{
					sm = SM_SIZE1;
				}
				break;
			case SM_SIZE1:
				{
					sm = SM_SIZE0;
				}
				break;
			case SM_SIZE0:
				{
					frame_datacnt      = buf_get_byte(2)*256+ch;
					frame_len_no_cksum = frame_datacnt+5;
					sm = SM_TOKEN;
				}
				break;
			case SM_TOKEN:
				{
					if(TOKEN==ch)
						sm = SM_DATA;
					else
						sm_reset();
				}
				break;
			case SM_DATA:
				{
					frame_datacnt--;
					if(0==frame_datacnt)
						sm = SM_CHECKSUM;
				}
				break;
			case SM_CHECKSUM:
				{
					return frame_len_no_cksum+1;
				}
				break;
			default:
				break;
		}
	}
}

void stk500v2_send_ack(uint8_t* frame_buffer, uint16_t frame_len_no_cksum)
{
	uint16_t frame_len = frame_len_no_cksum+1;
	frame_buffer[frame_len_no_cksum] = stk500v2_cal_checksum(frame_buffer, frame_len_no_cksum);
	for(uint16_t i=0; i<frame_len; i++)
	{
		uart_putbyte(frame_buffer[i]);
	}
}
//return sum of bytes in RX FIFO
uint8_t nrf_read_rx_fifo(uint8_t* pbuf)
{
	uint8_t sum = 0;
	while(!NRF_FIFO_STATUS_RXEMPTY(nrf_fifo_status()))
	{
		uint8_t wid = nrf_rx_payload_wid();
		nrf_rx_read_payload((char*)pbuf, (char)wid);
		pbuf += wid;
		sum += wid;
	}
	return sum;
}
//
//ret - ack frame length from nrf-bootloader  OR
//      0 -- sending timeout, nrf-bootloader not response
//      1 -- receive timeout, 
uint16_t stk500v2_cmd_execution(uint8_t* frame_buffer, uint16_t frame_len)
{
	#define SEND_RETRY_MAX 100
	#define RECV_RETRY_MAX 200
	uint16_t retry;
	uint8_t* frame_ptr = frame_buffer;
	//send frame data to nrf-bootloader
	nrf_clear();
	while(frame_len)
	{
		char cpsz = 32;
		uint8_t status;
		if(frame_len<32)
		{
			cpsz = (char)frame_len;
		}
		nrf_tx_write_payload((char*)frame_ptr, cpsz);
		frame_ptr += cpsz;
		frame_len -= cpsz;
		//
		retry = 0;
		status = 0x1E; //make a fake MAX-RT IRQ to enter loop
		while(NRF_STATUS_MAXRT(status)&&(retry<=SEND_RETRY_MAX))
		{
			if(retry>0)
			{
				LOG(LOG_LEVEL_NORMAL, "S-RT\r\n");
			}
			nrf_clear_irq_src(0x01); //clr MAX-RT IRQ
			nrf_sending_start();
			nrf_wait_for_irq();
			status = nrf_status();
			retry++;
		}
		if(retry>SEND_RETRY_MAX)
		{//failed for all re-trans
			LOG(LOG_LEVEL_NORMAL, "RT-MAX:%d\r\n", retry);
			return 0;
		}
		nrf_clear_irq_src(0x07);
	}
	//receive ACK frame from nrf-bootloader
	uint8_t  query = ~(uint8_t)MESSAGE_START;
	retry          = 0;
	frame_len      = 0;
	frame_ptr      = frame_buffer;
	while(retry<RECV_RETRY_MAX)
	{
		nrf_tx_write_payload((char*)&query, 1);
		nrf_sending_start(); //as PTX, must send one byte to get ACK payload from PRX
		nrf_wait_for_irq();
		if(NRF_STATUS_MAXRT(nrf_status()))
		{//no response, maybe ACK frame is not ready
			LOG(LOG_LEVEL_NORMAL, "RT %02X\r\n", nrf_fifo_status());
			retry++;
		}
		else
		{//query-byte send over
			uint8_t rxlen = nrf_read_rx_fifo(frame_ptr);
			frame_len += rxlen;
			frame_ptr += rxlen;
			if(rxlen)
			{//IRQ = TX_DS + RX_DR -- has ACK payload
				LOG(LOG_LEVEL_DEBUG, "MORE\r\n");
				retry = 0;//query more payload
			}
			else if(frame_len>0)
			{//TX_DS only ack frame buffer not empty
				LOG(LOG_LEVEL_DEBUG, "OVER\r\n");
				nrf_tx_flush();
				nrf_clear_irq_src(0x07);
				break;//end of ACK frame
			}
			else
			{//TX_DS only and ack frame buffer is empty
				//maybe ACK frame is not ready
				LOG(LOG_LEVEL_DEBUG, "GOON\r\n");
				retry++;//goon to query first piece of ACK frame
			}
		}
		nrf_tx_flush();
		nrf_clear_irq_src(0x07);
	}
	if(retry==RECV_RETRY_MAX)
	{//error
		LOG(LOG_LEVEL_NORMAL, "FAILED\r\n");
		nrf_clear();
		return 1;
	}
	return frame_len;
}

#if NRF_ENABLE_FREQ_HOPPING

uint8_t random_byte(void)
{
	uint8_t ret;
	_delay_ms(2);
	ADCSRA |= (1<<ADSC);       //start ADC Conversion : single conversion mode
	while(!(ADCSRA&(1<<ADIF)));//wait conversion complete
	ret = ADC&0xFF;            //read ADC value
	ADCSRA |= (1<<ADIF);       //clear ADC Interrupt Flag
	return ret;
}
void random_deinit(void)
{
	ADMUX  = 0x00;
	ADCSRA = 0x00;
}
//return: 0-illegal seed / >0-valid seed
uint16_t random_seed_generate(void)
{
	//Voltage Reference Selection:
	ADMUX  = ((0<<REFS1)|(1<<REFS0));   // AVCC, 5V on arduino UNO/Nano
	//Analog Channel Selection
	ADMUX |= (NRF_RANDOM_SEED_ADC_PIN&0x07); //select ADC pin
	//ADC Prescaler select: 
	//AVR ADC must be clocked at the frequency between 50 and 200kHz. 
	//So we need to set proper prescaller bits so that scaled system clock would fit in this range.
	//arduino at 16MHz, 128 scaling factor, 16000000/128=125kHz
	ADCSRA = ((1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0));
	//Enable ADC but disable ADC Interrupt
	ADCSRA |= ((1<<ADEN)|(0<<ADIE));
	//create seed
	uint8_t r0 = random_byte();
	uint8_t r1 = random_byte();
	uint8_t r2 = random_byte();
	if((r0==r1)&&(r1==r2))
	{
		return 0; //bad seed
	}
	else
	{
		return (r0<<8)|r1;
	}
}

struct _rf_hopping_cfg_
{
	uint8_t delay_ms;      //
	uint8_t rf_channel;
	uint8_t air_data_rate;
	uint8_t pipe_addr[5];  //five bytes, LSByte first   
} hopping_cfg;
// false - freq-hopping NOT finished
// true  - freq-hopping HAS BEEN finished
uint8_t hop_status_check(void)
{
	return (hopping_cfg.rf_channel<0x80);
}

void hop_config_clr(void)
{
	hopping_cfg.rf_channel = 0xFF;
}

void hop_config_reload(void)
{//create random cfg
	uint8_t rf;
	uint8_t a0,a1,a2,a3,a4;
	uint16_t seed = random_seed_generate();
	if(seed>0)
	{
		srand(seed);
		rf  = (uint8_t)(rand()%(2525-NRF_FREQ_HOPPING_LOWEST_CHANNEL));
		a0  = (uint8_t)(rand()%255);
		a1  = (uint8_t)(rand()%255);
		a2  = (uint8_t)(rand()%255);
		a3  = (uint8_t)(rand()%255);
		a4  = (uint8_t)(rand()%255);
		if((NRF_DEF_RF_CHANNEL-NRF_FREQ_HOPPING_LOWEST_CHANNEL)==rf)
		{
			rf -= 2; //skip the default RF channel
		}
		if((0x00==a0)&&(0x00==a1)&&(0x00==a2)&&(0x00==a3)&&(0x00==a4))
		{
			a0=a1=a2=a3=a4=(uint8_t)(~NRF_DEF_ADR_BYTE);
		}
	}
	else
	{
		rf = (NRF_DEF_RF_CHANNEL-NRF_FREQ_HOPPING_LOWEST_CHANNEL)-2;
		a0=a1=a2=a3=a4 = (uint8_t)(~NRF_DEF_ADR_BYTE);
	}
	random_deinit();
	LOG(LOG_LEVEL_NORMAL, "\r\nHOP-CFG: %02d-%02X %02X %02X %02X %02X\r\n", rf, a4, a3, a2, a1, a0);
	//
	hopping_cfg.rf_channel    = rf+(NRF_FREQ_HOPPING_LOWEST_CHANNEL-2400); //NRF_FREQ_HOPPING_LOWEST_CHANNEL~2524MHz
	hopping_cfg.pipe_addr[0]  = a4; //A4
	hopping_cfg.pipe_addr[1]  = a3; //A3
	hopping_cfg.pipe_addr[2]  = a2; //A2
	hopping_cfg.pipe_addr[3]  = a1; //A1
	hopping_cfg.pipe_addr[4]  = a0; //A0
	//
	hopping_cfg.delay_ms      = 5;

	#if NRF_FREQ_HOPPING_AIR_DATA_RATE_SET==0
		hopping_cfg.air_data_rate = NRF_VAL_RF_DR_2Mbps;
	#elif NRF_FREQ_HOPPING_AIR_DATA_RATE_SET==1
		hopping_cfg.air_data_rate = NRF_VAL_RF_DR_1Mbps;
	#elif NRF_FREQ_HOPPING_AIR_DATA_RATE_SET==2
		hopping_cfg.air_data_rate = NRF_VAL_RF_DR_250kbps;
	#endif
}
//1B 04 00 11 0E F0 05 00 02 C0 D0 02 C1 D1 06 C2 D0 D1 D2 D3 D4 00 CK 
//                   |     |____|  |______|  |_________________|  +---> end of config sequence
//                   |       |        |              |                     
//                   |       |        |              +------> nrf2401 config item2,LSByte-first for multi-bytes-data
//                   |       |        +------> nrf2401 config item1
//                   |       +-----> nrf2401 config item0
//                   +----> the bootloader should delay N ms before using this config sequence     
const uint8_t hop_seq_def[] PROGMEM = 
{ 
	MESSAGE_START, 0x00, 0x00, 0x11, TOKEN, CMD_NRF2401_FREQ_HOPPING, // data length: 17 bytes 
	0x00, // delay ms
	0x00, // reserve
	//set RF channel
	0x02, NRF_CMD_W_REG(NRF_REG_RF_CH), 0x00,
	//set air-data-rate
	0x02, NRF_CMD_W_REG(NRF_REG_RF_SETUP), NRF_VAL_RF_PWR_0dBm,
	//set pipe0 address, 5 bytes, LSBytes first
	0x06, NRF_CMD_W_REG(NRF_REG_RX_ADDR_P0), 0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 
	//END of config flow
	0x00
};
uint8_t hop_create_command(uint8_t* buf, uint8_t sz)
{
	flash_copy((uint16_t)hop_seq_def, buf, (uint16_t)sizeof(hop_seq_def));
	//byte[6] is delay ms
	buf[6]   = hopping_cfg.delay_ms;
	//byte[10] is data byte of RF channel 
	buf[10]  = hopping_cfg.rf_channel;
	//byte[13] is data byte of RF air-data-rate
	buf[13] |= hopping_cfg.air_data_rate;
	//byte[16]~byte[20] is pipe address 
	//LSBytes first
	buf[16]  = hopping_cfg.pipe_addr[4]; //A0
	buf[17]  = hopping_cfg.pipe_addr[3]; //A1
	buf[18]  = hopping_cfg.pipe_addr[2]; //A2
	buf[19]  = hopping_cfg.pipe_addr[1]; //A3
	buf[20]  = hopping_cfg.pipe_addr[0]; //A4
	//byte[22] is check-sum
	buf[22]  = stk500v2_cal_checksum(buf, 22);
	return 23; //frame length
}

uint8_t hopping_buffer[50];
void nrf_config_reset(void)
{//back to default set
	LOG(LOG_LEVEL_DEBUG, "RF-RESET\r\n");
	NRF_CE_LOW(); //go Standby-I
	nrf_set_rf_channel(NRF_DEF_RF_CHANNEL);
	nrf_set_rf_data_rate(NRF_AIR_DATA_RATE);
	nrf_set_auto_retrans(NRF_AUTO_RETRANS_DELAY, NRF_RETRANSMIT_MAX);
	nrf_set_default_addr();
}
void nrf_config_hopping(void)
{
	LOG(LOG_LEVEL_DEBUG, "RF-HOP\r\n");
	NRF_CE_LOW(); //go Standby-I
	//RF channel
	nrf_set_rf_channel(2400+hopping_cfg.rf_channel);
	//air-data-rate
	nrf_set_rf_data_rate(hopping_cfg.air_data_rate);
	//auto-retransmit
	if(hopping_cfg.air_data_rate==NRF_VAL_RF_DR_2Mbps)
		nrf_set_auto_retrans(NRF_VAL_ARD_500uS, NRF_RETRANSMIT_MAX);
	else if(hopping_cfg.air_data_rate==NRF_VAL_RF_DR_1Mbps)
		nrf_set_auto_retrans(NRF_VAL_ARD_750uS, NRF_RETRANSMIT_MAX);
	else if(hopping_cfg.air_data_rate==NRF_VAL_RF_DR_250kbps)
		nrf_set_auto_retrans(NRF_VAL_ARD_1750uS, NRF_RETRANSMIT_MAX);
	//addr-width
	nrf_set_addr_wid(NRF_VAL_AW_5Bytes);  
	//RX pipe0 addr: MSbytes first
	nrf_set_pipe_addr(NRF_PIPE0, (char*)(hopping_cfg.pipe_addr), sizeof(hopping_cfg.pipe_addr));
	//TX addr: MSbytes first
	nrf_set_tx_addr((char*)(hopping_cfg.pipe_addr), sizeof(hopping_cfg.pipe_addr));
	//delay as same as bootloader
	NRF_DELAY_mS(hopping_cfg.delay_ms+2);
}
#endif

uint8_t frame_buffer[300];

int main(void)
{
	cli();
	log_init();	
	uart_init();
	nrf_init();
#if NRF_ENABLE_PACKLOSS_CNT
	packloss_set(0);
	packsum_set(0);
#endif
#if NRF_ENABLE_FREQ_HOPPING
	hop_config_clr();
#endif
	while(1)
	{
		uint16_t frame_len = stk500v2_get_frame(frame_buffer, sizeof(frame_buffer));
		if(frame_buffer[frame_len-1]==stk500v2_cal_checksum(frame_buffer, frame_len-1))
		{//frame ok, goon
		#if NRF_ENABLE_FREQ_HOPPING
			if((CMD_SIGN_ON==frame_buffer[5])&&(!hop_status_check()))
			{
				uint16_t hop_cmd_len;
				hop_config_reload(); //create random config data
				hop_cmd_len = hop_create_command(hopping_buffer, (uint8_t)sizeof(hopping_buffer));
				hop_cmd_len = stk500v2_cmd_execution(hopping_buffer, hop_cmd_len);
				if(hop_cmd_len>1)
				{//bootloader has received the command
					nrf_config_hopping();
				}
			}
		#endif
			frame_len = stk500v2_cmd_execution(frame_buffer, frame_len);
			if(0==frame_len)
			{//error in [sending command frame]
				frame_buffer[6] = STATUS_CMD_TOUT;
				frame_len = 8;
			}
			else if(1==frame_len)
			{//error in [receiving ACK frame]
				frame_buffer[6] = STATUS_CMD_FAILED;
				frame_len = 8;
			}
		}
		else
		{//checksum error
			frame_buffer[6] = STATUS_CKSUM_ERROR;
			frame_len = 8;
		}
		stk500v2_send_ack(frame_buffer, frame_len-1);
		if(CMD_LEAVE_PROGMODE_ISP==frame_buffer[5])
		{
		#if NRF_ENABLE_FREQ_HOPPING
			hop_config_clr();
			nrf_config_reset();
		#endif
		#if NRF_ENABLE_PACKLOSS_CNT
			LOG(LOG_LEVEL_NORMAL, "package sum=%d , loss=%d\r\n", packsum_get(), packloss_get());
			packloss_set(0);
			packsum_set(0);
		#endif
		}
	
	}
	
}


