#include <avr/io.h>
#include <util/delay.h>
#include "nrf24_port.h"
//////////////////////////////////////////////////////////////
//          +--INT0[PD2]-[IN ] <====> IRQ----+             //
//          |--MISO[PB4]-[IN ] <====> MISO---|             //
// Arduino--|--MOSI[PB3]-[OUT] <====> MOSI---|---nRF24l01+ //
//          |--SCK [PB5]-[OUT] <====> SCK----|             //
//          |--SS  [PB2]-[OUT] <====> CSN----|             //
//          +--PB0 [PB1]-[OUT] <====> CE-----+             //
//////////////////////////////////////////////////////////////
//init SPI
void nrf_port_spi_init(void)
{
//init PORT 
//If PORTxn is written to '1' when the pin is configured as an input pin, 
//the pull-up resistor is activated. 
//bits:  7    6     5     4     3    2    1    0       
//DDRB:  0    0     1     0     1    1    1    0  : 0x2E
//PORTB: 0    0     0     1     0    1    0    0  : 0x14
	DDRB  = 0x2E;
	PORTB = 0x14;
	DDRD  = 0x00;
	PORTD = 0x04;
//init SPI
/* Enable SPI, Master, set clock rate fck/2 */
	SPCR = ((1<<SPE)|(1<<MSTR));
	SPSR = (1<<SPI2X);
	//init IRQ interrupt
	;;;
}
//time delay
void nrf_port_delay_us(unsigned char dly_us)
{
	//avr-gcc下不使用标准库提供的计时函数 _delay_us()
	//_delay_us()存在浮点运算，会导致代码尺寸变大
	//而且nrf24l01对计时精度要求不高,参考 _delay_us()的实现代码，此处使用简单的for循环实现
	for(;dly_us;dly_us--)
	{
		_delay_loop_1((unsigned char)((F_CPU)/3e6));
	}
}
//time delay
void nrf_port_delay_ms(unsigned char dly_ms)
{
	//参考 _delay_ms()的实现代码，此处使用简单的for循环实现
	for(;dly_ms;dly_ms--)
	{
		_delay_loop_2(((F_CPU)/4e3));
	}
}
//set CE pin high
void nrf_port_CE_high(void)
{//pin PB1
	PORTB |= (unsigned char)(1<<PIN1);
}
//set CE pin low
void nrf_port_CE_low(void)
{//pin PB1
	PORTB &= (unsigned char)(~(1<<PIN1));
}
//set CSN pin high
void nrf_port_CSN_high(void)
{//pin PB2
	PORTB |= (unsigned char)(1<<PIN2);
}
//set CSN pin low
void nrf_port_CSN_low(void)
{//pin PB2
	PORTB &= (unsigned char)(~(1<<PIN2));
}
//get status of IRQ pin
//return： 0   - IRQ low (active)
//         ~=0 - IRQ high (not active) 
char nrf_port_IRQ_level(void)
{//pin PD2
	return PIND&0x04;
}
//SPI write-read byte
//in  - byte to write
//return the byte read back
char nrf_port_spi_rw(char in)
{
	SPDR = in;                  //Start transmission
	while(!(SPSR&(1<<SPIF))); //Wait for transmission complete
	return SPDR;
}
